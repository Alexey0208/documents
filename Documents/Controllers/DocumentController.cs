﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Documents.Models;

namespace Documents.Controllers
{
    public class DocumentController : Controller
    {
        DHEntities db = new DHEntities();

        // GET: Document
        public ActionResult Index()
        {
            return View(db.Document1.ToList());
        }

        // GET: Document/Details/5
        public ActionResult Details(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document1 document = db.Document1.Find(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(document);
        }

        // GET: Document/Create
        public ActionResult Create()
        {
            Document1 model = new Document1();
            model.Дата = DateTime.Now;
            //ViewBag.DateToday = DateTime.Today;
            return this.View(model);
        }

        // POST: Document/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,Дата,Направление,Наименование_корсчета,Валюта,Наименование_клиента,Сумма,Комментарий,Статус")] Document1 document)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Document1.Add(document);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (DataException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Невозможно сохранить изменения. Проверьте данные еще раз");
            }
            return View(document);
            }

        // GET: Document/Edit/5
            public ActionResult Edit(int id)
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
                Document1 document = db.Document1.Find(id);
                if (document == null)
                {
                    return HttpNotFound();
                }
            return View(document);
            // return RedirectToAction("Index");
        }

        // POST: Document/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,Дата,Направление,Наименование_корсчета,Валюта,Наименование_клиента,Сумма,Комментарий,Статус")] Document1 document)
        {
            if (ModelState.IsValid)
            {
                db.Entry(document).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(document);
        }

        // GET: Document/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Document1 document = db.Document1.Find(id);
            if (document == null)
            {
                return HttpNotFound();
            }
            return View(document);
        }

        // POST: Document/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Document1 document = db.Document1.Find(id);
            db.Document1.Remove(document);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
//вот время это не нашел как убрать. И сразу текущую дату вбить
